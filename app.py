from src.config_loader import ConfigLoader
from src.input_data_loader import InputDataLoader
from src.pipeline_loader import PipelineLoader
from src.reporters.reporter import Reporter


def main():

    config = ConfigLoader('./config.yaml').load()
    reporter = Reporter(
        template_file='src/reporters/template/tsp-report_template.md',
        destination='./reports',
    )
    sentinel_data, points = InputDataLoader(config, reporter).load()
    pipeline = PipelineLoader(config['pipeline'], reporter).load()
    pipeline.process(sentinel_data, points)


if __name__ == '__main__':
    main()
