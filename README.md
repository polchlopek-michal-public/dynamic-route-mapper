# Dynamic route mapper


## Description
The app realizes my master's thesis **"Dynamic route mapping with use of runtime factor"**.  
Application has been implemented to generate the nearest path between n points and then after applying any product from Sentinel-3 satellite, which has been launched on earth's orbit by European Space Agency as a part Copernicus program.

## Structure 
The app  processes data in pipeline, there are two pipeline processing units, in which takes place calculation on data returned by unit's predecessor:
- TSP solver unit
- Satellite data processing unit  

![](docs/pipeline.png)

## Determining the shortest route
For generating nearest path between input points has been used the TSP algorithm with ordered crossover.
![](docs/tsp.png)


## Realtime factor
When TSP pipeline process unit finish path determination the satellite data are superimposed on the generated path. Then the custom algorithm modifies path to omit places with values determined in configuration.

![](docs/satelite_data.png)
![](docs/alternative_point.png)
![](docs/updated.png)