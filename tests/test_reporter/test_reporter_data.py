
source_data = """
# Header
{zero}

## Title_1:
one: {one}
two: {two}
three: {three}


## Title_2:
{four}
"""

loaded_data = """
# Header


## Title_1:
one: 
two: 
three: 


## Title_2:

"""

date_replaced_data = """
# Header
zero_replaced_data

## Title_1:
one: 
two: 
three: 


## Title_2:

"""

date_replaced_data_multiple_replaces = """
# Header
zero_replaced_data

## Title_1:
one: one_replaced_value
two: 
three: 


## Title_2:

"""

assigned_data = """
# Header
zero_replaced_data_1
zero_replaced_data_2
zero_replaced_data_3

## Title_1:
one: 
two: 
three: 


## Title_2:
four_replaced_data
"""
source_data = """
# Header


## Title_1:
one: 
two: 
three: 


## Title_2:
[!image](destination_dir/1.png)
"""