from unittest import TestCase, mock

from src.reporters.reporter import Reporter
from tests.test_reporter import test_reporter_data


class ReporterTestCase(TestCase):

    @mock.patch('src.reporters.reporter.open', new=mock.mock_open(read_data=test_reporter_data.source_data), create=True)
    def setUp(self) -> None:
        self.reporter = Reporter("./tests/test_reporter/test_template.md", "")

    def test_load_template(self):
        self.assertEqual(test_reporter_data.loaded_data,
                         self.reporter.content.data)

    def test_fmt_template(self):
        self.reporter["zero"].write("zero_replaced_data")
        self.assertEqual(test_reporter_data.date_replaced_data, self.reporter.content.data)

    def test_fmt_template_multiple_writes(self):
        self.reporter["zero"].write("zero_replaced_data")
        self.reporter["one"].write("one_replaced_value")
        self.assertEqual(test_reporter_data.date_replaced_data_multiple_replaces, self.reporter.content.data)

    def test_assign_to_variable(self):
        self.reporter["zero"].assign("zero_replaced_data_1")
        self.reporter["zero"].assign("zero_replaced_data_2")
        self.reporter["zero"].assign("zero_replaced_data_3")
        self.reporter["four"].assign("four_replaced_data")
        self.assertEqual(test_reporter_data.assigned_data, self.reporter.content.data)
        