import os
import shutil
import yaml
import logging


class ConfigLoader:
    def __init__(self, path: str = "./") -> None:
        super().__init__()
        self.path = path

    def load(self):
        if not os.path.exists(self.path):
            shutil.copy("src/config/config_template.yaml", self.path)
        with open(self.path, "r") as stream:
            try:
                return yaml.safe_load(stream)
            except Exception as error:
                logging.error(f"Parsing config file failed: {error}")
