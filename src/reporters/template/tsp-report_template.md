# TSP Problem solution report
{timestamp}

## Model hiperparameters:
locations amount: {locations_amount}  
generations amount: {generations}  
population size: {population}  
crossover probability : {crossover_probability}  
roulette selection k factor: {roulette_selection_k}  
mutation probability: {mutation_probability}  
hall of fame size: {hall_of_fame_size}  


## Optimization process record:
{optimization_process}


## Sentinel output process unit:
{sentinel_output_process_unit}