import datetime
import os
import re
import typing

import PIL
from matplotlib import figure


class Content:
    data: str = ""
    files_counter: int = 0


class Item:
    def __init__(
        self,
        key: str,
        occurrences: typing.Dict[str, typing.List[int]],
        content: Content,
        dst_dir: str,
        data_subdir: str
    ) -> None:
        self.key = key
        self.occurrences = occurrences
        self.content = content
        self.dst_dir = dst_dir
        self.data_subdir = data_subdir

    def _update_remaining_occurrences(self, content: str, shift: int = 0):
        for occurrence_position in self.occurrences.values():
            if occurrence_position[0] > self.occurrences[self.key][0]:
                occurrence_position[0] += len(content) + shift
                occurrence_position[1] += len(content) + shift

    def _update_on_overwrite(self, content: str):
        self.occurrences[self.key][1] = self.occurrences[self.key][0] + \
            len(content)
        self._update_remaining_occurrences(content)

    def _update_on_assignment(self, content: str):
        self.occurrences[self.key][1] = self.occurrences[self.key][1] + \
            len(content) + 1
        self._update_remaining_occurrences(content, 1)

    def _content(self, *args):
        return str(*args[0]) if args[0] else None

    def _plot(self, fig: figure.Figure):

        self.content.files_counter += 1
        dst = f'{self.dst_dir}/{self.data_subdir}/{self.content.files_counter}.png'
        fig.savefig(dst)
        return f'![image]({self.data_subdir}/{self.content.files_counter}.png)'

    def write(self, *args, plot=None) -> None:
        content = self._content(args)
        if plot != None:
            content = self._plot(plot)
        if content == None:
            return

        self.content.data = \
            self.content.data[:self.occurrences[self.key][0]] \
            + content \
            + self.content.data[self.occurrences[self.key][1]:]
        self._update_on_overwrite(content)

    def assign(self, *args, plot=None) -> None:
        content = self._content(args)
        if plot != None:
            content = self._plot(plot)
        if content == None:
            return

        if self.occurrences[self.key][0] == self.occurrences[self.key][1]:
            self.content.data = \
                self.content.data[:self.occurrences[self.key][1]] \
                + f'{content}' \
                + self.content.data[self.occurrences[self.key][1]:]
            self._update_on_overwrite(content)
        else:
            self.content.data = \
                self.content.data[:self.occurrences[self.key][1]] \
                + f'\n{content}' \
                + self.content.data[self.occurrences[self.key][1]:]
            self._update_on_assignment(content)


class Reporter:

    def __init__(self, template_file: str, destination: str) -> None:
        self.content_file: str = template_file
        self.destination: str = destination
        self.replacements: typing.Dict[str, typing.List[int, int]] = {}
        self.occurrences = {}
        self.content: Content = Content()
        self.timestamp: str = str(datetime.datetime.now()) \
            .replace(' ', '-') \
            .replace(':', '-') \
            .replace('.', '-')
        self.reported = False

        with open(self.content_file) as template_file:
            self.content.data = template_file.read()

        self.dir_final_path = f'{self.destination}/{self.timestamp}'
        self.data_subdir = 'data'
        self.data_full_path = f'{self.dir_final_path}/{self.data_subdir}'
        
        if not os.path.exists(self.dir_final_path):
            os.makedirs(self.dir_final_path)
        if not os.path.exists(self.data_full_path):
            os.makedirs(self.data_full_path)

        bias = 0
        for match in re.finditer(r'\{.*\}', self.content.data):
            self.occurrences[match.group()[
                1:-1]] = [match.start() - bias, match.start()-bias]
            self.content.data = self.content.data[:match.start(
            )-bias] + self.content.data[match.end()-bias:]
            bias += len(match.group())

    def __getitem__(self, key: str):
        return Item(key, self.occurrences, self.content, self.dir_final_path, self.data_subdir)

    def report(self) -> None:
        self.reported = True
        with open(f"{self.dir_final_path}/report.md", "w") as file:
            file.write(self.content.data)
    
    def path(self) -> str:
        return self.dir_final_path

    def __del__(self):
        if not self.reported:
            self.report()
