import importlib


class SatelliteData:
    def of(config):
        class_ = getattr(importlib.import_module(
            f"src.satellite_data.readers.satellite_{config['satellite-instrument']}_data_reader"), f"Satellite{config['satellite-instrument'].capitalize()}DataReader")
        instance = class_()
        return instance.read(
            config['satellite-data-path'],
            config['satellite-product-name'],
            config['satellite-product-file'],
        )
