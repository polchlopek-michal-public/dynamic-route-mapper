import xarray as xr
from abc import ABC, abstractmethod

class ISatelliteDataReader(ABC):
    
    @abstractmethod
    def read(self, path: str, product_name: str, product_file: str) -> xr.DataArray:
        raise NotImplementedError()
