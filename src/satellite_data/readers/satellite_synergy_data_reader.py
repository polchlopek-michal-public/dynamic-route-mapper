import xarray as xr
from src.satellite_data.readers.i_satellite_data_reader import ISatelliteDataReader


class SatelliteSynergyDataReader(ISatelliteDataReader):

    def read(self, path: str, product_name: str, product_file: str) -> xr.DataArray:
        data: xr.Dataset = xr.open_dataset(f"{path}/{product_file}")
        data = data.rename({product_name: 'value'})
        return data
