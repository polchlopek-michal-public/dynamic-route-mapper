import xarray as xr
from src.satellite_data.readers.i_satellite_data_reader import ISatelliteDataReader


class SatelliteSlstrDataReader(ISatelliteDataReader):

    def read(self, path: str, product_name: str, product_file: str) -> xr.DataArray:

        geo_data: xr.Dataset = xr.open_dataset(f"{path}/geodetic_in.nc")
        data: xr.Dataset = xr.open_dataset(f"{path}/{product_file}")

        data = data.rename({product_name: 'value'})
        data = data.assign_coords({
            "longitude": (["rows", "columns"], geo_data.longitude_in.data),
            "latitude": (["rows", "columns"], geo_data.latitude_in.data)
        })
        return data
