import copy
import math
import random
import typing

import numpy as np
from deap import base, creator, tools


class TSPModel:

    def evaluate(self, individual: np.ndarray):
        path_len = 0
        for index in range(1, len(individual)):
            path_len += math.dist((self.points.loc[individual[index], "longitude"], self.points.loc[individual[index], "latitude"]),
                                  (self.points.loc[individual[index-1], "longitude"], self.points.loc[individual[index-1], "latitude"]))
        return path_len

    def individual_to_points(self, individual):
        return [self.points[index] for index in individual]

    def generation_to_points(self, generation):
        pointsPopulation = []
        for individual in generation:
            pointsIndividual = self.individual_to_points(individual)
            pointsPopulation += [pointsIndividual]
        return pointsPopulation

    def generate_indices(self):
        arr = np.arange(1, self.points.shape[0]-1)
        np.random.shuffle(arr)
        arr = np.append(0, arr)
        arr = np.append(arr, self.points.shape[0]-1)
        return arr

    def cx_ordered_with_freezed_endpoints(self, ind1: np.ndarray, ind2: np.ndarray, inplace=True):
        child1 = ind1[1:-1]-1
        child2 = ind2[1:-1]-1
        child1, child2 = tools.cxOrdered(child1, child2)
        child1 = child1+1
        child2 = child2+1

        child1 = np.append(0, child1)
        child1 = np.append(child1, self.points.shape[0]-1)

        child2 = np.append(0, child2)
        child2 = np.append(child2, self.points.shape[0]-1)
        if inplace:
            ind1[:] = child1
            ind2[:] = child2
        return child1, child2

    def mut_shuffle_with_freezed_endpoints(self, ind, mutation_probability, inplace=True):

        child = ind[1:-1]-1
        child, = tools.mutShuffleIndexes(child[:], mutation_probability)
        child = child+1

        child = np.append(0, child)
        child = np.append(child, self.points.shape[0]-1)

        if inplace:
            ind[:] = child

        return child,

    def best_path(self):
        return self.individual_to_points(self.best_individual)

    def __init__(
        self,
        *,
        points: np.ndarray,
        on_generation_callback: typing.Callable,
        config,
    ) -> None:
        self.points = points
        self.on_generation_callback = on_generation_callback
        self.generations = config['generations']
        self.population = config['population']
        self.crossover_probability = config['crossover-probability']
        self.roulette_selection_k = config['roulette-selection-k']
        self.mutation_probability = config['mutation-probability']
        self.hall_of_fame_size = config['hall-of-fame-size']

        self.best_individual = None
        self.best_updated = False

        self.toolbox = base.Toolbox()
        creator.create("FitnessMin", base.Fitness, weights=(-1.0,))
        creator.create("Individual", np.ndarray, fitness=creator.FitnessMin)

        self.toolbox.register("indices",
                              self.generate_indices
                              )

        self.toolbox.register("individual",
                              tools.initIterate,
                              creator.Individual, self.toolbox.indices)

        self.toolbox.register("population",
                              tools.initRepeat,
                              list,
                              self.toolbox.individual,
                              )

        self.toolbox.register("mate",
                              self.cx_ordered_with_freezed_endpoints
                              )

        self.toolbox.register("mutate",
                              self.mut_shuffle_with_freezed_endpoints
                              )

        self.toolbox.register("select", tools.selTournament, tournsize=3)
        self.toolbox.register("evaluate", self.evaluate)

        self.hof = tools.HallOfFame(
            self.hall_of_fame_size,
            similar=lambda x, y: np.equal(x, y).all()
        )

        self.pop = self.toolbox.population(n=self.population)

    def fit(self):

        # Evaluate the entire population
        fitnesses = self.toolbox.map(self.toolbox.evaluate, self.pop)
        for ind, fit in zip(self.pop, fitnesses):
            ind.fitness.values = (fit,)
        for generation in range(self.generations):
            offspring = self.toolbox.select(
                self.pop, k=3)  # selection
            offspring = copy.deepcopy(offspring)

            # crossover
            for child1, child2 in zip(offspring[0:len(offspring)//2], offspring[len(offspring)//2:]):
                if random.random() < self.crossover_probability:
                    self.toolbox.mate(child1, child2)
                    del child1.fitness.values
                    del child2.fitness.values

            # mutation
            for child in offspring:
                self.toolbox.mutate(child, self.mutation_probability)

            # Evaluate the individuals with an invalid fitness
            invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
            fitnesses = map(self.toolbox.evaluate, invalid_ind)
            for ind, fit in zip(invalid_ind, fitnesses):
                ind.fitness.values = (fit,)

            self.pop.sort(key=lambda x: x.fitness, reverse=True)

            self.best_updated = False
            if self.best_individual is None or np.less(self.pop[0].fitness.values, self.best_individual.fitness.values).all():
                self.best_updated = True
                self.best_individual = copy.deepcopy(self.pop[0])

            self.hof.update(self.pop[:])
            # self.pop = np.concatenate((self.pop, offspring),axis=0)
            self.pop[-self.hall_of_fame_size:] = offspring
            self.on_generation_callback(
                self.points.iloc[self.best_individual], self.best_updated)

        return self.pop
