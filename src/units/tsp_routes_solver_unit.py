import copy
import typing
import pickle
import xarray as xr
import pandas as pd
import matplotlib.pyplot as plt

from src.pipeline.pipeline_unit import PipelineUnit
from src.tsp_model import TSPModel
from src.reporters.reporter import Reporter
from src.utils.container import Container


class TspRoutesSolverUnit(PipelineUnit):
    def __init__(self, config, reporter: Reporter) -> None:
        super().__init__()
        self.config = config
        self.reporter = reporter

    def __on_tsp_evaluation_callback(self, best, best_updated: bool, sentinel_data, output_points):
        if best_updated == False:
            return
        fig, ax = plt.subplots(figsize=(20, 10))
        x = []
        y = []

        for _, row in best.iterrows():
            x.append(row["latitude"])
            y.append(row["longitude"])

        sentinel_data["value"].plot(
            x="longitude", y="latitude", ax=ax, alpha=0.7)
        ax.plot(x, y)

        self.reporter["optimization_process"].assign(plot=fig)
        output_points.value = copy.deepcopy(best)

    def process(self, sentinel_data: xr.Dataset, points: pd.DataFrame) -> typing.Tuple[xr.Dataset, pd.DataFrame]:
        output_points = Container(None)
        tsp_model = TSPModel(
            points=points,
            config=self.config['model-params'],
            on_generation_callback=lambda pop, best_updated: self.__on_tsp_evaluation_callback(
                pop,
                best_updated,
                sentinel_data,
                output_points,
            )
        )

        tsp_model.fit()

        pickle.dump(output_points.value, open(
            f'{self.reporter.path()}/data/output_points.obj', 'wb'))
        pickle.dump(sentinel_data, open(
            f'{self.reporter.path()}/data/sentinel_data.obj', 'wb'))

        return sentinel_data, output_points
