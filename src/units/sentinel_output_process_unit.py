import typing
import math
import xarray as xr
import pandas as pd
import matplotlib.pyplot as plt

from src.pipeline.pipeline_unit import PipelineUnit
from src.reporters.reporter import Reporter


class SentinelOutputProcessUnit(PipelineUnit):
    def __init__(self, config, reporter: Reporter) -> None:
        super().__init__()
        self.config = config
        self.reporter = reporter

    def process(self, sentinel_data: xr.Dataset, points: pd.DataFrame) -> typing.Tuple[xr.Dataset, pd.DataFrame]:
        points = self.__process_sections(sentinel_data, points)
        self.__report_plot(sentinel_data, points)
        return sentinel_data, points

    def __process_sections(self, sentinel_data: xr.DataArray, points: pd.DataFrame) -> pd.DataFrame:
        splitted_path: pd.DataFrame = pd.DataFrame(
            {"longitude": [], "latitude": []}
        )
        for indx in range(0, points.value.shape[0] - 1):
            splitted_path = pd.concat([splitted_path, self.__process_single_sections(
                points.value.iloc[indx], points.value.iloc[indx+1], sentinel_data)])
        splitted_path = splitted_path.reset_index()
        return splitted_path

    def __split_path(self, a, b) -> pd.DataFrame:
        splitted_path: pd.DataFrame = pd.DataFrame(
            {"longitude": [], "latitude": []}
        )
        ax = a['longitude']
        ay = a['latitude']
        bx = b['longitude']
        by = b['latitude']
        lx = abs(bx-ax)
        ly = abs(by-ay)
        l = (lx**2 + ly**2)**(1/2)
        n = int(l//self.config['section-length'])
        dx = 0 if n == 0 else lx/n
        dy = 0 if n == 0 else ly/n

        # direction of path
        dx_factor = 1 if bx > ax else -1
        dy_factor = 1 if by > ay else -1

        for i in range(0, n):
            splitted_path = pd.concat([splitted_path, pd.DataFrame({"longitude": [
                                      a['longitude'] + i*dx*dx_factor], "latitude": [a['latitude'] + i*dy*dy_factor]})])
        return splitted_path

    def __process_single_sections(self, a, b, sentinel_data) -> pd.DataFrame:
        return self.__process_path(sentinel_data, self.__split_path(a, b))

    def __process_path(self, sentinel_data: xr.Dataset, points: pd.DataFrame) -> pd.DataFrame:
        points = points.reset_index()
        for indx, point in points.iterrows():
            if indx == 0:
                continue
            mean_val = self.__pixels_mean_value(sentinel_data, point)
            if mean_val < self.config['threshold']:
                points = self.__recalculate_path(
                    indx, sentinel_data, point, points)
            return points

    def __recalculate_path(self, indx: int, sentinel_data: xr.Dataset, point, points: pd.DataFrame) -> pd.DataFrame:
        x = point['latitude']
        y = point['longitude']
        dest_point = points.iloc[-1]
        best = None
        max_mean_val = None
        l = self.config['section-length']
        alternative_points = pd.DataFrame({
            "latitude":     [x+1, x+1, x,   x-1, x-1, x-1, x,   x+1],
            "longitude":    [y,   y-1, y-1, y-1, y,   y+1, y+1, y+1]
        })

        for _, evaluated_point in alternative_points.iterrows():
            mean_val = self.__evaluate_point(
                point, evaluated_point, points.iloc[-1], sentinel_data
            )
            if (max_mean_val == None) or (mean_val > max_mean_val if self.config["optimization"] == "max" else mean_val < max_mean_val):
                max_mean_val = mean_val
                best = evaluated_point

        path = pd.concat(
            [points.iloc[0:indx], self.__split_path(best, dest_point)])
        path.reset_index()
        return path

    def __evaluate_point(self, orig_point, evaluated_point, dest_point, sentinel_data):
        x1 = orig_point['latitude']
        y1 = orig_point['longitude']
        x2 = evaluated_point['latitude']
        y2 = evaluated_point['longitude']
        x_dest = dest_point['latitude']
        y_dest = dest_point['longitude']

        direction_factor = self.config['direction-factor']
        mean_value_factor = self.config['mean_values_factor']

        origin_path_vector = [x_dest - x1, y_dest - y1]
        evaluated_path_vector = [x_dest - x2, y_dest - y2]
        dot_product = origin_path_vector[0]*evaluated_path_vector[0] + \
            origin_path_vector[1] * evaluated_path_vector[1]
        mod_of_vectors = math.sqrt(
            origin_path_vector[0]**2 + evaluated_path_vector[1]**2) * math.sqrt(evaluated_path_vector[0]**2 + evaluated_path_vector[1]**2)
        angle = dot_product / mod_of_vectors

        return direction_factor * math.acos((angle)) + mean_value_factor * self.__pixels_mean_value(sentinel_data, evaluated_point)

    def __pixels_mean_value(self, sentinel_data: xr.DataArray, point) -> float:
        x = point['latitude']
        y = point['longitude']
        delta = self.config['section-length'] / 2
        data = sentinel_data.where(
            (sentinel_data.value.longitude > (x - delta)) &
            (sentinel_data.value.longitude < (x + delta)) &
            (sentinel_data.value.latitude > (y - delta)) &
            (sentinel_data.value.latitude < (y + delta)), drop=True)
        return float(data.mean().value.data)

    def __report_plot(self, sentinel_data: xr.Dataset, points: pd.DataFrame):
        fig, ax = plt.subplots(figsize=(20, 10))

        sentinel_data["value"].plot(
            x="longitude", y="latitude", ax=ax, alpha=0.7)
        ax.plot(points['latitude'], points['longitude'])

        self.reporter["sentinel_output_process_unit"].write(plot=fig)
