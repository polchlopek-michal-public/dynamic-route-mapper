import typing
import pickle
import yaml
import xarray as xr
import pandas as pd

from src.reporters.reporter import Reporter
from src.satellite_data.satellite_data import SatelliteData


class InputDataLoader:
    def __init__(self, config: yaml.YAMLObject,  reporter: Reporter) -> None:
        super().__init__()
        self.config = config
        self.reporter = reporter

        # factories method for input data load
        self.factories = {"fresh": lambda: self.__fresh,
                          "persistance": lambda: self.__persistance,
                          }

    def __fresh(self):
        points: pd.DataFrame = pd.read_csv(
            self.config['input-data']['points']['data-path']
        )
        sentinel_data: xr.DataArray = SatelliteData.of(
            self.config['input-data']['satellite']
        )

        if self.config['input-data']['constraints']['max-lon'] != None \
                and self.config['input-data']['constraints']['min-lon'] != None \
                and self.config['input-data']['constraints']['max-lat'] != None \
                and self.config['input-data']['constraints']['min-lat'] != None:
            sentinel_data = sentinel_data.where(
                sentinel_data.value.longitude >
                self.config['input-data']['constraints']['min-lon'], drop=True)
            sentinel_data = sentinel_data.where(
                sentinel_data.value.longitude <
                self.config['input-data']['constraints']['max-lon'], drop=True)
            sentinel_data = sentinel_data.where(
                sentinel_data.value.latitude >
                self.config['input-data']['constraints']['min-lat'], drop=True)
            sentinel_data = sentinel_data.where(
                sentinel_data.value.latitude <
                self.config['input-data']['constraints']['max-lat'], drop=True)

        return sentinel_data, points

    def __persistance(self):
        points = pickle.load(
            open(self.config['input-data']['persistance']['route-points-path'], 'rb'))

        sentinel_data = pickle.load(
            open(self.config['input-data']['persistance']['sentinel-data-path'], 'rb'))

        return sentinel_data, points

    def load(self) -> typing.Tuple[xr.Dataset, pd.DataFrame]:
        return self.factories[self.config['input-data']['source']]()()
