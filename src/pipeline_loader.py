import os
import importlib

from src.pipeline.pipeline import Pipeline


class PipelineLoader:
    def __init__(self, config, reporter) -> None:
        super().__init__()
        self.config = config
        self.reporter = reporter

    def load(self) -> Pipeline:
        units = []
        for key, _ in self.config.items():
            module_name = key.replace('-', '_')
            class_name = key.title().replace('-', '')
            class_ = None
            if os.path.exists(f"src/units/{module_name}.py"):
                class_ = getattr(importlib.import_module(
                    f"src.units.{module_name}"), class_name)
            elif os.path.exists(f"src/units/{module_name}/{module_name}.py"):
                class_ = getattr(importlib.import_module(
                    f"src.units.{module_name}.{module_name}"), class_name)
            if class_ != None:
                units.append(class_(self.config[key], self.reporter))
        return Pipeline(units)
