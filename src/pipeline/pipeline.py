import typing
import xarray as xr
import pandas as pd

from src.pipeline.pipeline_unit import PipelineUnit


class Pipeline():
    def __init__(self, units: typing.List[PipelineUnit]) -> None:
        self.units: typing.List[PipelineUnit] = units

    def process(self, sentinel_data: xr.Dataset, points: pd.DataFrame) -> typing.Tuple[xr.Dataset, pd.DataFrame]:
        return self._process_unit(0, sentinel_data, points)

    def _process_unit(self, index: int, sentinel_data: xr.Dataset, points: pd.DataFrame) -> typing.Tuple[xr.Dataset, pd.DataFrame]:
        try:
            sentinel_data, units = self.units[index].process(
                sentinel_data, points)
            return self._process_unit(index+1, sentinel_data, units)
        except IndexError as e:
            return sentinel_data, points
