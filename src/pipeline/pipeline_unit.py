import typing
import xarray as xr
import pandas as pd

from abc import ABC, abstractmethod


class PipelineUnit(ABC):

    @abstractmethod
    def process(self, sentinel_data: xr.Dataset, points: pd.DataFrame) -> typing.Tuple[xr.Dataset, pd.DataFrame]:
        pass
